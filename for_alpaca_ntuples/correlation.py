#!/usr/bin/python3
from __future__ import print_function
import sys
import time
from timeit import default_timer as timer
import logging 
import argparse
import itertools
import uproot

#iname="/eos/user/c/crizzi/RPV/alpaca/results/alpaca_8j_hydra_3layers_UDS_1400/outputtree_test.root"
iname="/eos/user/c/crizzi/RPV/alpaca/results/alpaca_8j_hydra_3layers_UDS_1400/outputtree_test_dijet_FTinfo.root"
tname="tree"

loose=False

nom_iter = uproot.pandas.iterate(iname, tname,entrysteps=float('inf'))#,branches=[''])
for df  in nom_iter:    
    df['asymmetry'] =  (df['mt1_reco']-df['mt2_reco']).abs()/ (df['mt1_reco']+df['mt2_reco'])
    columns = df.columns
    columns = [c.replace('t1','g1').replace('t2','g2') for c in columns]
    df.columns = columns
    print(df.head())
    corr = df.corr(method='pearson')
    for c in corr.columns:
        corr[c]=corr[c].abs()
    print(corr['mg1_reco'].sort_values())

    if loose:
        # from here try new approach
        names = list(corr[corr.mg1_reco<0.02].index)
        names = [n for n in names if not n == 'njets_25']
        print(names)
        corr = corr[eval(" | ".join(["(corr['{0}'] < 0.02)".format(col) 
                                     for col in names]))]
        print(corr)
        #
        
        #corr = corr[names]
        #for c in names:
        #    corr[c] = corr[c].abs()
        #print(corr)
        #print(corr[['mt1_reco']])
    else:
        names = list(corr.index)+['mg1_reco']
        corr = corr[corr.mg1_reco<0.02]
        names = list(corr.index)+['mg1_reco']
        names = [n for n in names if not n == 'njets_25']
        print(names)
        corr = corr[names]
        for c in names:
            corr[c] = corr[c].abs()
        print(corr)
        #print(corr[['mg1_reco']])

