#!/usr/bin/python3
from __future__ import print_function
import sys
import os
import time
from timeit import default_timer as timer
import logging 
import argparse
import ROOT as R
import itertools

R.gROOT.SetBatch(1)
R.gStyle.SetOptStat(0)
R.gStyle.SetOptTitle(0)

colors = [R.kAzure+1, R.kPink+1, R.kOrange+1, R.kGreen+2, R.kMagenta-7, 
          R.kRed-4, R.kBlue+1]

wei='weight'
#wei='normweight*mcEventWeight'

#plots_dir_general='/eos/user/c/crizzi/www/RPV_MJ/FT_022621/'
plots_dir_general='./plots_new'

write_std=['ATLAS Internal, Simulation']

selections = []
#selections.append(('pass_sel', 'presel', write_std+["HT > 1100 GeV, pT(0)>100 GeV"]))
selections.append(('ht_50>1000 && jet_pt_0>100 && jet_pt_5>50', 'presel', write_std+["HT > 1100 GeV, pT(0)>100 GeV"," pT(5)>50 GeV"],1))
#selections.append(('ht_50>2000 && jet_pt_0>100 && jet_pt_5>50', 'ht2000', write_std+["HT > 2000 GeV, pT(0)>100 GeV","pT(5)>50 GeV"]))
#selections.append(('njets>=10 && jet_pt_0>600 && ht_50>2000 && jet_pt_5>100 &&  D_32_t2<=0.45 && jet_pt_5/jet_pt_0>0.01'
#                   , 'tight', write_std+["N(J)>=10, pT(0)>600 GeV,","pT(5)>100 GeV, HT > 2000 GeV,", "D32(g2)<0.45"]))
#selections.append(('njets>=10 && jet_pt_0>600 && ht_50>2000 && jet_pt_5>300'
#                   , 'pt5300', write_std+["N(J)>=10, pT(0)>600 GeV,","pT(5)>300 GeV, HT > 2000 GeV,"], 10))
#selections.append(('Sum$(jet_pt*(jet_pt>50))>1000 && jet_pt[0]>100 && jet_pt[5]>50', 'presel', write_std+["HT > 1100 GeV, pT(0)>100 GeV"," pT(5)>50 GeV"], 1#))
#selections.append(('Sum$(jet_pt*(jet_pt>50))>1000 && jet_pt[0]>100 && jet_pt[5]>50', 'presel', write_std+["HT > 1100 GeV, pT(0)>100 GeV"," pT(5)>50 GeV"], 1))

tag='alpaca_8j_hydra_3layers_UDS_1400'
#tag='alpaca_sigbkg_12j_simpleNN_3layers_UDBUDS_1400'
files_grouped = []
files_grouped.append(
    ('sig_bkg',
     [
         ("/eos/user/c/crizzi/RPV/alpaca/results/"+tag+"/outputtree_test_dijet.root","tree",'dijet', '1', 1./0.3),
         #("/eos/user/c/crizzi/RPV/alpaca/results/"+tag+"/outputtree_test.root","tree",'1400-correct-match', "(alpaca_good_mt1>0 && alpaca_good_mt2>0)", 1./0.12),
         ("/eos/user/c/crizzi/RPV/alpaca/results/"+tag+"/outputtree_test.root","tree",'1400-all', '1', 1./0.12)
     ])
)
#files_grouped.append(
#    ('sig_bkg',
#     [
#         ("/eos/user/c/crizzi/RPV/ntuples/022621/output/mc16e/qcd_cleaned.root","trees_SRRPV_",'dijet', '1', 1.),
#         ("/eos/user/c/crizzi/RPV/ntuples/022621/output/mc16e/signal/504539.root","trees_SRRPV_",'1400', '1', 1.)
#     ])
#)
#files_grouped.append(
#    ('NN_sig_bkg',
#        [
#            ("/eos/user/c/crizzi/RPV/alpaca/results/"+tag+"/NNoutput_test.root","tree",'dijet', 'tagged_true==0', 1./0.3),
#            ("/eos/user/c/crizzi/RPV/alpaca/results/"+tag+"/NNoutput_test.root","tree",'1400', 'tagged_true==1', 1./0.3), 
#        ])
#)

variables = [
    ('Sum$(jet_isSig>-1)', (21, -0.5, 20.5), 'Number of baseline jets', 'njets_baseline'),
    ('Sum$(jet_isSig>0)', (21, -0.5, 20.5), 'Number of signal jets', 'njets_signal'),
    ('Sum$((jet_isSig>0)*(jet_QGTagger_tagged_WPq80<1))', (21, -0.5, 20.5), 'Number of signal quark jets', 'njets_signal_quark'),
    ('Sum$((jet_isSig>0)*(jet_QGTagger_tagged_WPq80>0))', (21, -0.5, 20.5), 'Number of signal gluon jets', 'njets_signal_gluon'),
]

variables_score = [
    #('tagged', (100, 0, 1), 'score', 'score'+tag)
    #('sqrt(jet_px_0*jet_px_0 + jet_py_0*jet_py_0)', (100, 0, 2000), 'p_{T} leading jet [GeV]', 'jet_pt_0'),
]

variables = [
    #('jet_pt_5:ht_50', (80, 1000, 5000, 20, 50, 450), 'p_{T} 6th jet [GeV]:HT (jets>50 GeV) [GeV]', 'jet_pt_5_vs_ht'),
    #('jet_pt_5:njets', (15, 5.5, 20.5, 20, 50, 450), 'p_{T} 6th jet [GeV]:Number of baseline jets', 'jet_pt_5_vs_njets'),
    #('ht_50:njets', (15, 5.5, 20.5, 80, 1000, 5000), 'HT (jets>50 GeV) [GeV]:Number of baseline jets', 'ht_vs_njets'),
    #('(jet_pt_0-jet_pt_5)/ht_50', (70, 0, 0.7), '(p_{T} leading jet - p_{T} 6th jet) / H_{T}', 'jet_pt_0_minus_jet_pt_5_over_HT'),
    #('(jet_pt_5)/ht_50', (50, 0, 0.2), 'p_{T} 6th jet / H_{T}', 'jet_pt_over_HT'),
    #('jet_pt_0', (100, 0, 2000), 'p_{T} leading jet [GeV]', 'jet_pt_0'),
    #('jet_pt_5', (100, 0, 400), 'p_{T} 6th jet [GeV]', 'jet_pt_5'),
    #('jet_pt_5/jet_pt_0', (100, 0, 1), 'p_{T} 6th jet / p_{T} leading jet', 'jet_pt_5_over_jet_pt_0'),
    #('mt1_true', (80, 0, 2000), 'm(g1) TRUE [GeV]', 'mg1_true'),
    ('mt1_reco', (80, 0, 2000), 'm(g1) RECO [GeV]', 'mg1_reco'),
    #('dphi_t1_t2', (100, 0, 3.5), '#Delta#phi(g1,g2)', 'dphi_g1_g2' ),
    #('dR_t1_t2', (100, 0, 8), '#DeltaR(g1,g2)', 'dR_g1_g2' ),
    #('dphi_t1_min', (100, 0, 3.5), 'Min #Delta#phi(jets in g1)', 'dphi_g1_min' ),
    #('dphi_t1_max', (100, 0, 3.5), 'Max #Delta#phi(jets in g1)', 'dphi_g1_max' ),
    #('dphi_t2_min', (100, 0, 3.5), 'Min #Delta#phi(jets in g2)', 'dphi_g2_min' ),
    #('dphi_t2_max', (100, 0, 3.5), 'Max #Delta#phi(jets in g2)', 'dphi_g2_max' ),
    #('dR_t1_min', (100, 0, 5), 'Min #DeltaR(jets in g1)', 'dR_g1_min' ),
    #('dR_t1_max', (100, 0, 5), 'Max #DeltaR(jets in g1)', 'dR_g1_max' ),
    #('dR_t2_min', (100, 0, 5), 'Min #DeltaR(jets in g2)', 'dR_g2_min' ),
    #('dR_t2_max', (100, 0, 5), 'Max #DeltaR(jets in g2)', 'dR_g2_max' ),
    #('dphi_min_j31_j32', (100, 0, 3.5), 'Min(#Delta#phi(j2,j0), #Delta#phi(j2,j1))', 'dphi_min_j20_j21'),
    #('dR_min_j31_j32', (100, 0, 5), 'Min(#DeltaR(j2,j0), #DeltaR(j2,j1))', 'dR_min_j20_j21'),
    #('(mt1_reco - mt2_reco)', (40, -1000, 1000), 'm(g1) - m(g2) RECO [GeV]', 'mg1_minus_mg2_reco'),
    #('(mt1_reco + mt2_reco)', (40, 1000, 4000), 'm(g1) + m(g2) RECO [GeV]', 'mg1_plus_mg2_reco'),
    #('mt2_reco', (80, 0, 2000), 'm(g2) RECO [GeV]', 'mg2_reco'),
    #('fabs(mt2_reco-mt1_reco)/(mt2_reco+mt1_reco)', (100, 0, 0.8), 'Asymmetry', 'asymmetry'),
    #('mt1_random', (80, 0, 2000), 'm(g1) RANDOM [GeV]', 'mg1_random'),
    #('mt2_random', (80, 0, 2000), 'm(g2) RANDOM [GeV]', 'mg2_random'),
    #('njets', (15, 5.5, 20.5), 'Number of baseline jets', 'njets'),
    #('ht', (100, 1000, 5000), 'HT (all jets) [GeV]', 'ht'),
    #('ht_50', (100, 1000, 5000), 'HT (jets>50 GeV) [GeV]', 'ht_50'),
    #('ht_8j', (100, 1000, 5000), 'HT (8 leading jets) [GeV]', 'ht_8j'),
    #('score_is_from_tt_1', (80, 0.6, 1), 'score is from gg g1', 'score_is_from_gg_g1'),
    #('score_is_from_tt_2', (80, 0.6, 1), 'score is from gg g2', 'score_is_from_gg_g2'),
    #('score_same_as_lead_1', (80, 0.6, 1), 'score same as lead g1', 'score_same_as_lead_g1'),
    #('score_same_as_lead_2', (80, 0.6, 1), 'score same as lead g2', 'score_same_as_lead_g2'),
    #('score_sum', (80, 0.6, 1), 'sum of scores', 'score_sum'),
    #('D_32_t1', (100, 0, 1), 'D(32) g1', 'D_32_g1'),
    #('D_32_t2', (100, 0, 1), 'D(32) g2', 'D_32_g2'),
    #('m_63_ijk[0]', (100, 0, 0.5), 'm(63) g1', 'm_63_g1'),
    #('m_63_ijk[1]', (100, 0, 0.5), 'm(63) g2', 'm_63_g2'),
    #('D2', (80, 0, 0.8), 'D^{2} g1 g2', 'D2'),    
]



'''
# piece of code needed to make a file list with different selections on the same plot
# e.g. 
# commented out to create streamlined plotting code, but I need to think of how to put it back!
sel1=[('njets<=8','_nj_leq_8'),('njets>=9','_nj_geq_9')]
sel2=[('D_32_t2>0.45','_D_32_t2_g_0p45'),('D_32_t2<=0.45','_D_32_t2_l_0p45')]
selections = [(x,y) for x in sel1 for y in sel2]
#print(selections)
files_tmp = [
    ["/eos/user/c/crizzi/RPV/alpaca/results/alpaca_8j_hydra_3layers_UDS_1400/outputtree_test_dijet_FTinfo.root","tree", 'dijet'],
]

files=[]
for f in files_tmp:
    for s in selections:
        f_appo = f.copy()
        sel = s[0][0] + ' && '+s[1][0]
        print(sel)
        l = s[0][1]  +s[1][1]
        f_appo[-1] = f_appo[-1]+l
        f_appo.append(sel)
        print(f_appo)
        files.append(f_appo)

#for f in files:
#    print(f)
'''

for group_name, files in files_grouped:
    print(selections[0])
    for sel_base, suff, write_base, divide_bins in selections:
        #continue
        for v in variables:
            hs = []
            ftest = R.TFile('test_appo.root','recreate')
            for i,f in enumerate(files):
                #print(f[2])
                f_t = R.TFile.Open(f[0])
                t = f_t.Get(f[1])
                # print(t)
                nentries = t.GetEntries()
                h_name = 'h_'+str(f[2])+'_'+v[3]
                if not ':' in v[0]:
                    if not "njets" in v[0]:
                        hs.append(R.TH1F(h_name, h_name, int(v[1][0]/divide_bins), v[1][1], v[1][2]))
                    else:
                        hs.append(R.TH1F(h_name, h_name, v[1][0], v[1][1], v[1][2]))
                else:
                    hs.append(R.TH2F(h_name, h_name, v[1][0], v[1][1], v[1][2], v[1][3], v[1][4], v[1][5]))
                if len(f)>3:
                    sel = '('+sel_base+')*('+f[3]+')'
                sel = '('+wei+')*('+sel+')'
                #print(v[0])
                #print(sel)
                t.Draw(v[0]+' >> '+h_name, sel, 'goff')
                print(v[0]+' >> '+h_name)
                print(hs)
                print(hs[i])        
                print("Integral:", hs[i].Integral())
                if len(f)>4:
                    hs[i].Scale(f[4])
                ftest.cd()
                hs[i].Write()
            f_t.Close()
        
                
            ftest.Close()
            for unit_area in [True, False]:
                h_max =0 
                write = write_base + ['1 pb^{-1}'] if not unit_area else write_base
                unit_area_suff = 'unit_area' if unit_area else 'xsec'
                plots_dir = plots_dir_general + '/' + group_name + '/' + suff + '/' + unit_area_suff + '/'
                #print(plots_dir)
                os.makedirs(plots_dir, exist_ok=True)
                fi = R.TFile.Open('test_appo.root')
                leg = R.TLegend(0.6,0.71,0.89,0.89)

                for i,f in enumerate(files):
                    h_name = 'h_'+str(f[2])+'_'+v[3]
                    h = fi.Get(h_name)
                    if h.Integral()>0 and unit_area:
                        h.Scale(1./h.Integral())
                    if h.GetMaximum()>h_max: 
                        h_max = h.GetMaximum()

                if not ':' in v[0]:
                    c = R.TCanvas('c_'+v[3]+group_name+str(unit_area)+'_'+suff,'c_'+v[3]+group_name+str(unit_area)+'_'+suff)
                    c.cd()
                for i,f in enumerate(files):
                    if ':' in v[0]:
                        c = R.TCanvas('c_'+v[3]+group_name+str(unit_area)+'_'+suff+'_'+f[2],'c_'+v[3]+group_name+str(unit_area)+'_'+suff+'_'+f[2])
                        c.cd()                        
                    h_name = 'h_'+str(f[2])+'_'+v[3]
                    #print(h_name)
                    h = fi.Get(h_name)
                    if h.Integral()>0 and unit_area:
                        h.Scale(1./h.Integral())
                    if not ':' in v[0]:
                        h.GetXaxis().SetTitle(v[2])
                        h.SetMaximum(h_max*1.4)
                        if not unit_area:
                            h.SetMaximum(h_max*500)
                            h.SetMinimum(0.000001)
                    else:
                        h.GetXaxis().SetTitle(v[2].split(':')[1])
                        h.GetYaxis().SetTitle(v[2].split(':')[0])
                        h.SetMinimum(0.000001)
                        if not unit_area:
                            h.SetMaximum(10)
                    if not ':' in v[0]:
                        h.SetLineColor(colors[i])
                        h.SetLineWidth(3)
                        if "match" in f[2]:
                            h.SetLineColorAlpha(colors[i],0.55)
                        h.GetYaxis().SetTitle("Number of events")
                        if unit_area: 
                            h.GetYaxis().SetTitle("Events normalized to unit area")
                    h.GetYaxis().SetTitleFont(43)
                    h.GetYaxis().SetTitleSize(19)
                    h.GetYaxis().SetLabelFont(43)
                    h.GetYaxis().SetLabelSize(15)
                    h.GetYaxis().SetTitleOffset(1.1)        
                    h.GetXaxis().SetTitleFont(43)
                    h.GetXaxis().SetTitleSize(19)
                    h.GetXaxis().SetLabelFont(43)
                    h.GetXaxis().SetLabelSize(15)
                    h.GetXaxis().SetTitleOffset(1.)  
                    
                    if not ':' in v[0]:
                        if i>0: h.Draw('same')
                        else: h.Draw()
                        leg_str = str(f[2])
                        leg_str = leg_str.replace('_',' ').replace(' g ','>').replace(' l ','<').replace('0p','0.').replace(' leq ','\leq').replace(' geq ','\geq')
                        if '1400' in leg_str: leg_str='#splitline{RPV 6-jet model}{(#tilde{g} = 1.4 TeV)}'
                        leg.AddEntry(h, leg_str)

                    else:
                        h.Draw('COLZ')

                    text =  R.TLatex()
                    text.SetNDC()
                    text.SetTextAlign( 11 )
                    text.SetTextFont( 42 )
                    text.SetTextSize( 0.045 )
                    text.SetTextColor( 1 )
                    y = 0.84
                    #write.append(region.replace("_","-"))
                    for t in write:
                        text.DrawLatex(0.13,y, t)
                        y = y-0.055
                    #text.DrawLatex(0.15,y, var[0].replace("_","-"))
                    c.Update()

                    if ':' in  v[0]:
                        name_can = plots_dir+'/'+v[3]+'_'+f[2]
                        c.SetLogz()
                        #c.SaveAs(name_can+'.png')
                        c.SaveAs(name_can+'.pdf')

                if not ':' in v[0]:
                    leg.SetLineColor(0)
                    leg.SetFillStyle(0)
                    leg.Draw()
                    if not unit_area:
                        c.SetLogy()
                    name_can = plots_dir+'/'+v[3]
                    #c.SaveAs(name_can+'.png')
                    c.SaveAs(name_can+'.pdf')
                fi.Close()
    
