#!/usr/bin/python3
from __future__ import print_function
import sys
import time
from timeit import default_timer as timer
import logging 
import argparse
import itertools
import uproot
import math

eos='/eos/atlas/atlascerngroupdisk/phys-susy/RPV_mutlijets_ANA-SUSY-2019-24/ntuples/workshop/'
iname = eos+'FT/mc16e/qcd_cleaned.root'

#iname = '/eos/atlas/atlascerngroupdisk/phys-susy/RPV_mutlijets_ANA-SUSY-2019-24/ntuples/workshop/FT/mc16e/signal/504544.root'

tname = 'trees_SRRPV_'
nom_iter = uproot.pandas.iterate(iname, tname,entrysteps=float('inf'), flatten=False)#,branches=[''])
#nom_iter = uproot.pandas.iterate(iname, tname,entrysteps=float('inf'),branches=['Sum$(jet_pt)'])
for df  in nom_iter:
    #print(df.columns)
    #print(df.head)
    #print(df[['jet_pt','jet_eta']])
    df['ht'] = df['jet_pt'].apply(lambda x: sum(x))
    df=df[df['ht']>1500]
    df['nj'] = df['jet_pt'].apply(lambda x: len(x))
    df['nj_80'] = df['jet_pt'].apply(lambda x: len([j for j in x if j>80 ]))
    df['ht_80'] = df['jet_pt'].apply(lambda x: sum([j for j in x if j>80 ]))
    df['a14'] = df['jet_pt'].apply(lambda x: (x[0]-x[3])/(x[0]+x[3]))
    df['a16'] = df['jet_pt'].apply(lambda x: (x[0]-x[5])/(x[0]+x[5]))
    df['pt6'] = df['jet_pt'].apply(lambda x: x[5])
    df['eta1'] = df['jet_eta'].apply(lambda x: math.fabs(x[0]))
    df['eta2'] = df['jet_eta'].apply(lambda x: math.fabs(x[1]))
    df['deta12'] = df['jet_eta'].apply(lambda x: math.fabs(x[0]-x[1]))
    df['etamax'] = df['jet_eta'].apply(lambda x: max([math.fabs(j) for j in x]))
    df=df[['ht','ht_80','nj', 'nj_80','a14','a16','pt6','eta1','deta12','etamax']]
    print('all dataset')
    corr = df.corr(method='pearson')    
    print(corr)
    print('ht>1500')
    corr = df[df['ht']>1500].corr(method='pearson')    
    print(corr)
