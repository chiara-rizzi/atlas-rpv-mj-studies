#####################################################
# Check dR between quarks from the same neutralino  #
# contact: chiara.rizzi@cern.ch                     #
#####################################################

from __future__ import print_function
import sys
import time
from timeit import default_timer as timer
import logging 
import argparse
import ROOT as R
import itertools

R.gROOT.SetBatch(1)
R.gStyle.SetOptStat(0)
R.gStyle.SetOptTitle(0)

colors = [R.kPink+1, R.kAzure+1, R.kOrange+1, R.kGreen+2, R.kMagenta-7, 
          R.kRed-4, R.kBlue+1]


# list of input FT ntuples in the format (path-to-file, tree-name, gluino-mass)
'''
files = [
    ("../ntuple_prod/WorkArea/run/submit_dir_900/data-trees/DAOD_SUSY4.24001508._000001.pool.root.1.root","trees_SRRPV_",900),
    ("../ntuple_prod/WorkArea/run/submit_dir_1600/data-trees/DAOD_SUSY4.24001435._000001.pool.root.1.root","trees_SRRPV_",1600),
    ("../ntuple_prod/WorkArea/run/submit_dir_2200/data-trees/DAOD_SUSY4.24001793._000001.pool.root.1.root","trees_SRRPV_",2200),
]
'''

files = [
    ("/eos/user/c/crizzi/RPV/ntuples/210324c/output/signal/403615.root","trees_SRRPV_",1950)
]


# output file where to store the dR histograms
fout = R.TFile('min_and_max_dR.root','recreate')

for f in files:
    print(f[0])
    print(f[1])
    f_t = R.TFile.Open(f[0])
    t = f_t.Get(f[1])
    print(t)
    h_min = R.TH1F('h_mindR_'+str(f[2]), 'h_mindR_'+str(f[2]), 30, 0, 3)
    h_max = R.TH1F('h_maxdR_'+str(f[2]), 'h_maxdR_'+str(f[2]), 60, 0, 6)
    print(h_min)
    nentries = t.GetEntries()
    # per-neutralino quantities
    N_min_less_0p4 = 0
    N_min_less_0p8 = 0
    N_max_less_1p0 = 0
    # per-event quantities
    min_less_0p4 = 0
    min_less_0p8 = 0
    max_less_1p0 = 0
    ientry_skip = [14280, 10706]
    for ientry in range(nentries):
        print(ientry)
        if ientry in ientry_skip: continue
        t.GetEntry(ientry)
        # print t.truth_QuarkFromGluino_ParentBarcode
        N1 = [] # list of quarks from first neutralino
        N2 = [] # list of quarks from second neutralino
        for iq in range(len(t.truth_QuarkFromNeutralino_ParentBarcode)):
            e = t.truth_QuarkFromNeutralino_e[iq]
            eta = t.truth_QuarkFromNeutralino_eta[iq]
            pt = t.truth_QuarkFromNeutralino_pt[iq]
            phi = t.truth_QuarkFromNeutralino_phi[iq]
            q = R.TLorentzVector()
            q.SetPtEtaPhiE(pt, eta, phi, e)
            if iq == 0: # put the first quark in the list of the first neutralino
                ref_parent = t.truth_QuarkFromNeutralino_ParentBarcode[iq]
                N1.append(q)
            else: # put in the list of the first cluino if same parent as first quark, else in the list of second neutralino
                if t.truth_QuarkFromNeutralino_ParentBarcode[iq] == ref_parent:
                    N1.append(q)
                else:
                    N2.append(q)
        # print(len(N1), len(N2))
        min_N1 = 999
        min_N2 = 999
        max_N1 = 0 
        max_N2 = 0 
        pairs = list(itertools.combinations(range(3), 2))
        # find min and max dR in first neutralino
        for p in pairs:
            q1 = N1[p[0]]
            q2 = N1[p[1]]
            dR = q1.DeltaR(q2)
            if dR < min_N1: min_N1 = dR
            if dR > max_N1: max_N1 = dR
        # find min and max dR in second neutralino
        for p in pairs:
            q1 = N2[p[0]]
            q2 = N2[p[1]]
            dR = q1.DeltaR(q2)
            if dR < min_N2: min_N2 = dR
            if dR > max_N2: max_N2 = dR
        # print('N1  min:', min_N1, '  max:',max_N1)
        # print('N2  min:', min_N2, '  max:',max_N2)
        # update per-event quantities
        min_less_0p4 += 1 if min(min_N1, min_N2) < 0.4 else 0
        min_less_0p8 += 1 if min(min_N1, min_N2) < 0.8 else 0
        max_less_1p0 += 1 if min(max_N1, max_N2) < 1.0 else 0
        # update per-neutralino quantities
        if min_N1 < 0.4: N_min_less_0p4 += 1
        if min_N2 < 0.4: N_min_less_0p4 += 1
        if min_N1 < 0.8: N_min_less_0p8 += 1
        if min_N2 < 0.8: N_min_less_0p8 += 1
        if max_N1 < 1.0: N_max_less_1p0 += 1
        if max_N2 < 1.0: N_max_less_1p0 += 1
        # fill histograms
        h_min.Fill(min_N1)
        h_min.Fill(min_N2)
        h_max.Fill(max_N1)
        h_max.Fill(max_N2)

    # write histograms to file
    fout.cd()
    h_min.Write()
    h_max.Write()

    # print to screen percentages
    print("\nNeutralino mass:", f[2], "GeV")  
    print("Per-neutralino results")
    print("neutralinos with min < 0.4:", "{:.1f}".format( 100*float(N_min_less_0p4)/(2.0*nentries)),"%")
    print("neutralinos with min < 0.8:", "{:.1f}".format( 100*float(N_min_less_0p8)/(2.0*nentries)),"%")
    print("neutralinos with max < 1.0:", "{:.1f}".format( 100*float(N_max_less_1p0)/(2.0*nentries)),"%")
    print("Per-event results (at least one neutralino with ...)")
    print("events with min < 0.4:", "{:.1f}".format( 100*float(min_less_0p4)/(nentries)),"%")
    print("events with min < 0.8:", "{:.1f}".format( 100*float(min_less_0p8)/(nentries)),"%")
    print("events with max < 1.0:", "{:.1f}".format( 100*float(max_less_1p0)/(nentries)),"%")

# close output file
fout.Close()

# open output file (reading mode) and plot histograms
fi = R.TFile.Open('min_and_max_dR.root','read')
c = R.TCanvas()
leg = R.TLegend(0.6,0.7,0.9,0.9)
for i,f in enumerate(files):
    c.cd()
    print(f)
    h = fi.Get('h_mindR_'+str(f[2]))
    h.GetXaxis().SetTitle('min-dR')
    print(h)
    h.SetLineWidth(3)
    h.SetLineColor(colors[i])
    h.Scale(1./h.Integral())
    h.Draw('same')
    leg.AddEntry(h, str(f[2]))
leg.Draw()
c.SaveAs('mindR_all.pdf')

c2 = R.TCanvas()
leg2 = R.TLegend(0.6,0.7,0.9,0.9)
for i,f in enumerate(files):
    c2.cd()
    print(f)
    h = fi.Get('h_maxdR_'+str(f[2]))
    h.GetXaxis().SetTitle('max-dR')
    print(h)
    h.SetLineWidth(3)
    h.SetLineColor(colors[i])
    h.Scale(1./h.Integral())
    h.Draw('same')
    leg2.AddEntry(h, '#Deltam = '+str(f[2]))
leg2.Draw()
c2.SaveAs('maxdR_all.pdf')
