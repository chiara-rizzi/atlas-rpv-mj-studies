import ROOT as R

#path='/eos/atlas/atlascerngroupdisk/phys-susy/RPV_mutlijets_ANA-SUSY-2019-24/ntuples/workshop/FT/mc16e/'
path = '/eos/user/c/crizzi/RPV/ntuples/workshop/output/mc16e/'
iname = path+'qcd.root'
oname = path+'qcd_cleaned.root'

infile = R.TFile.Open(iname)
outfile = R.TFile.Open(oname,'RECREATE')
tree_name='trees_SRRPV_'

sel = '(((jet_pt[0]+jet_pt[1])/2.) < 1.4*truth_jet_pt[0])'

tree = infile.Get(tree_name)
tree.SetBranchStatus("*",1)
newtree = tree.CopyTree(sel)
newtree.SetName(tree.GetName())
outfile.cd()
newtree.Write()
outfile.Close()
