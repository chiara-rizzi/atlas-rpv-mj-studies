from __future__ import print_function
import ROOT
import json

#eos='/eos/user/c/crizzi/RPV/ntuples/022621/output/'
#infile_name = eos+'/mc16e/qcd.root'
#infile_data_name = eos + '/data/data18_periodI.root'
tree_name = 'trees_SRRPV_'
#folder = '/eos/user/c/crizzi/www/RPV_MJ/FT_022621/data_mc/'

#eos='/eos/atlas/atlascerngroupdisk/phys-susy/RPV_mutlijets_ANA-SUSY-2019-24/ntuples/workshop/'
#infile_name = eos+'FT/mc16e/qcd_cleaned.root'

#infile_data_name = eos + '/data_processed/data15.root'
#tree_name = 'trees_SRRPV_'
#folder = '/eos/user/c/crizzi/www/RPV_MJ/workshop/data_mc/'

eos='/eos/user/c/crizzi/RPV/ntuples/timev5/output/'
infile_name = eos+'qcd.root'
folder='.'

var = ['jet_pt[0]',(100,0,1000),'p_{T} leading jet [GeV]','jet_pt_0']
#var = ['jet_pt[0]',(140,100,1500),'p_{T} leading jet [GeV]','jet_pt_0_zoom']
#var = ['Sum$(jet_pt*(jet_pt>50))',(100,1000,5000),'H_{T} [GeV]','ht_50']
#var = ['Sum$(jet_pt)',(100,1000,6000),'H_{T} [GeV]','ht']
#var = ['TVector2::Phi_mpi_pi(jet_phi[0]-jet_phi[1])',(80,0,4),'dphi leading two jets','dphi_leading2_jets']
#var = ['@jet_pt.size()',(16,4.5,20.5),'Number of baseline jets','number_of_baseline_jets']
#var = ['Sum$(jet_isSig>-1)',(16,4.5,20.5),'Number of baseline jets','number_of_baseline_jets_v2']
#var = ['Sum$(jet_isSig>0)',(16,4.5,20.5),'Number of signal jets','number_of_signal_jets']
#var = ['Sum$((jet_isSig>0)*(jet_QGTagger_tagged_WPg80<1))',(16,4.5,20.5),'Number of signal quark-tagged jets','number_of_signal_quark_jets']
#dsid = range(364701,364713)
dsid = range(364701,364705)

use_data = False
#lumi_data = 1
lumi_data = 800.0
weights = '('+str(lumi_data)+'*normweight*mcEventWeight)'

#write=['ATLAS Internal, period I 2018 (308 pb^{-1})','HLT_ht1000, HT>1.1 TeV', ' pT(0)>350 GeV, pT(5)>50 GeV']
#write=['ATLAS Internal, period I 2018 (800 pb^{-1})','HLT_ht1000, HT>1.1 TeV', ' pT(5)>50 GeV']
write=['Test']
#base_sel = 'jet_pt[5]>50 && pass_HLT_ht1000_L1J100 && Sum$(jet_pt*(jet_pt>50))>1100'
base_sel = '1'

do_slices = True

print([d for d in dsid])

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptTitle(0)
ROOT.gStyle.SetOptStat(0)

thstr = '(((jet_pt[0]+jet_pt[1])/2.) < 1.4*truth_jet_pt[0])'
#thstr = '(1)'
print(thstr)

if use_data:
    infile_data = ROOT.TFile.Open(infile_data_name)
    t_data = infile_data.Get(tree_name)
    print(t_data.GetEntries())
    h_data = ROOT.TH1F('h_data','h_data',var[1][0], var[1][1],var[1][2])
    sel = base_sel
    t_data.Draw(var[0]+'>>h_data',sel,'goff')    

infile = ROOT.TFile.Open(infile_name)
t = infile.Get(tree_name)
print(t.GetEntries())

nevents={}
if do_slices:
    h_list = []
    for i,d in enumerate(dsid):
        print(d)
        h = ROOT.TH1F('h_'+str(d),'h_'+str(d), var[1][0], var[1][1],var[1][2])
        sel = base_sel + " && mcChannelNumber==" + str(d)
        t.Draw(var[0]+'>>h_'+str(d), '('+weights+')*('+sel+ ' && '+thstr +  ')','goff')
        h_list.append(h)    
    h_tot = h_list[0].Clone('h_tot')
    for h in h_list[1:]:
        h_tot.Add(h)
else:
    h_tot = ROOT.TH1F('h_tot','h_tot', var[1][0], var[1][1],var[1][2])
    sel = base_sel 
    t.Draw(var[0]+'>>h_tot', '('+weights+')*('+sel+ ' && '+thstr +  ')','goff')

h_tot.SetLineColor(ROOT.kBlue)
h_tot.SetLineWidth(3)
h_tot.GetXaxis().SetTitle(var[2])
if use_data:
    h_tot.SetMaximum(h_tot.GetMaximum()*70)
else:
    h_tot.SetMaximum(h_tot.GetMaximum()*1000)
h_tot.GetYaxis().SetTitleFont(43)
h_tot.GetYaxis().SetTitleSize(19)
h_tot.GetYaxis().SetLabelFont(43)
h_tot.GetYaxis().SetLabelSize(15)
h_tot.GetYaxis().SetTitleOffset(1.3)        
h_tot.GetXaxis().SetTitleFont(43)
h_tot.GetXaxis().SetTitleSize(19)
h_tot.GetXaxis().SetLabelFont(43)
h_tot.GetXaxis().SetLabelSize(15)
h_tot.GetXaxis().SetTitleOffset(1.5)  
h_tot.GetYaxis().SetTitle("Number of events")


leg=ROOT.TLegend(0.75,0.41,0.89,0.89)
leg.SetFillStyle(0)
leg.SetLineColor(0)

colors = [410, 856, 607, 801, 629, 879, 602, 921, 622, 410, 856, 607, 801, 629, 879]
if do_slices:
    for i,h in enumerate(h_list):
        h.SetLineColor(colors[i])
        leg.AddEntry(h, str(dsid[i]),'l')
leg.AddEntry(h_tot, 'Dijet tot','l')
#leg.AddEntry(h_2b, '==2b', "l")
#leg.AddEntry(h_2bMass, '==2b && 100<m(bb)<150', "l")

c = ROOT.TCanvas("can","can",600,600)
c.cd()
if not use_data:
    pad1 = ROOT.TPad("pad1", "pad1",0.0,0.0,1.0,1.0,22)
else:
    pad1 = ROOT.TPad("pad1", "pad1",0.0,0.35,1.0,1.0,21)
    pad2 = ROOT.TPad("pad2", "pad2",0.0,0.0,1.0,0.35,22)
    pad2.SetFillColor(0)
    pad2.Draw()
    pad2.SetGridy()
pad1.SetFillColor(0)
pad1.Draw()

pad1.SetTicky()
pad1.SetLogy()
#c.SetLogx()
pad1.cd()

h_tot.Draw('hist')
if do_slices:
    for h in h_list:
        h.Draw('same hist')
if use_data:
    h_data.SetMarkerStyle(20)
    leg.AddEntry(h_data, 'data')
    #if len(h_list)>0:
    h_data.Draw('E0 same')
    #else:
    #    h_data.Draw('E0')
leg.Draw()
# pad1.RedrawAxis()
pad1.RedrawAxis("g")

text =  ROOT.TLatex()
text.SetNDC()
text.SetTextAlign( 11 )
text.SetTextFont( 42 )
text.SetTextSize( 0.05 )
text.SetTextColor( 1 )
y = 0.82
#write.append(region.replace("_","-"))
for t in write:
    text.DrawLatex(0.15,y, t)
    y = y-0.06
    #text.DrawLatex(0.15,y, var[0].replace("_","-"))
pad1.Update()

if use_data:
    pad2.cd()
    pad2.SetTicky()
    h_ratio = h_data.Clone("h_ratio")
    h_ratio.Divide(h_tot)
    h_ratio.GetYaxis().SetTitleFont(43)
    h_ratio.GetYaxis().SetTitleSize(19)
    h_ratio.GetYaxis().SetLabelFont(43)
    h_ratio.GetYaxis().SetLabelSize(15)
    h_ratio.GetYaxis().SetTitleOffset(1.3)        
    h_ratio.GetYaxis().SetTitle("Data/MC")
    h_ratio.SetLineColor(1)
    h_ratio.SetMinimum(0.4)
    h_ratio.SetMaximum(1.6)
    h_ratio.Draw()
    hline=ROOT.TH1D("hline", "hline", 1, var[1][1],var[1][2])
    hline.SetBinContent(1,1)
    hline.GetYaxis().SetTitle("Data/MC")
    hline.Draw("same")


    pad2.RedrawAxis("g")
    pad2.Update()

c.SaveAs(folder+'/'+var[3]+'.png')
