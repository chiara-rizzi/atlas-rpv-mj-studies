#################################################
# Signal efficiency plot                        #
# contact: chiara.rizzi@cern.ch                 #
#################################################

from __future__ import print_function
import sys
import time
from timeit import default_timer as timer
import logging 
import argparse
import ROOT as R
import itertools

import json
import glob
from array import array

R.gROOT.SetBatch(1)
R.gStyle.SetOptStat(0)
R.gStyle.SetOptTitle(0)

path = '/eos/user/c/crizzi/RPV/ntuples/FT_signal_020321_merged/mc16e/signal/*root'
did2group_name = 'did2group.json'
tree_name = 'trees_SRRPV_'
#sel = 'jet_pt[0]>350 && jet_pt[5]>50 && pass_HLT_ht1000_L1J100 && jet_pt[5]/jet_pt[0]>0.01'
#pdf_name = 'all_sel.pdf'
#sel = 'jet_pt[0]>0 && jet_pt[5]>50 && pass_HLT_ht1000_L1J100 && jet_pt[5]/jet_pt[0]>0.01'
#pdf_name = 'all_sel_nopT.pdf'
#sel = 'jet_pt[5]>50 && pass_HLT_ht1000_L1J100 && jet_pt[5]/jet_pt[0]>0.01 && jet_pt[0]>350'
#sel = 'pass_HLT_6j45_gsc55_boffperf_split_0eta240_L14J15>0 && jet_pt[5]>60'
#pdf_name = 'eff_6jtrigger_pt5_60.pdf'
#write=['ATLAS Internal','pass_HLT_6j45_gsc55_boffperf_split_0eta240_L14J15','p_{T}^{J5}>60 GeV']

#sel = 'pass_HLT_ht1000_L1J100 && Sum$(jet_pt)>1100 && jet_pt[0]>200'
sel = 'pass_HLT_ht1000_L1J100 && Sum$(jet_pt*(jet_pt>50))>1100 && jet_pt[0]>200'
pdf_name = 'eff_HTtrigger_HT1100_leadjet200_ht50.pdf'
write=['ATLAS Internal','pass_HLT_ht1000_L1J100','HT>1100 GeV, p_{T}^{J0}>200 GeV']

#weights = 'mcEventWeight*weight_lumi_real'
weights = 'mcEventWeight*normweight'
#write=['ATLAS Internal','HLT_ht1000_L1J100, p_{T}^{J0}>350 GeV,','p_{T}^{J5}>50 GeV, p_{T}^{J5}/p_{T}^{J0}>0.01']


with open(did2group_name) as f:
    did2group = json.load(f)
print(did2group)

l = sorted(glob.glob(path))
print(l)
# all_groups = [ (did, file-name) ] 
all_groups = [(did2group[x.split('/')[-1].replace('.root','')],x) for x in l]
# groups = [(group)] (without mass) 
groups = set([ '_'.join( x[0].split('_')[:-1]) for x in all_groups ])
# print(groups)

by_group = dict()
for g in groups:
    by_group[g]=[]
for gr,f in all_groups:
    for g in groups:
        if g in gr:
            by_group[g].append((gr.split('_')[-1],f))
            
print(by_group)
c = R.TCanvas("can","can",800,600)
y = dict()
x = dict()
gr = dict()
y_min = 999
y_max = -999
x_min = 999999
x_max = -999
for g in by_group:
    print(g)
    y[g] = array( 'd' )
    x[g] = array( 'd' )
    for m,f in by_group[g]:
        m = float(m)
        print(m)
        x[g].append(m)
        ft = R.TFile.Open(f,'read')
        t = ft.Get(tree_name)
        h_name = 'h_'+g+'_'+str(m)
        h = R.TH1F(h_name,h_name,1, 0,2)
        draw_str = '(('+sel+')*('+weights+'))'
        draw_str_den = '((1)*('+weights+'))'
        t.Draw('1 >> '+h_name, draw_str, 'goff')
        num = h.Integral()
        t.Draw('1 >> '+h_name, draw_str_den, 'goff')
        den = h.Integral()
        eff = num/den
        print(num, den, eff)
        y[g].append(eff)
        if eff>y_max: y_max = eff
        if eff<y_min: y_min = eff
        if m>x_max: x_max = m
        if m<x_min: x_min = m
    gr[g] = R.TGraph(len(x[g]),x[g],y[g])

    
c.cd()
leg = R.TLegend()
margin = 0.5*(y_max-y_min)
h=c.DrawFrame(0.95*x_min, y_min-margin, 1.05*x_max, margin+y_max)
h.GetXaxis().SetTitle('m(gluino)  [GeV]')
h.GetYaxis().SetTitle('Efficiency')
h.GetYaxis().SetTitleFont(43)
h.GetYaxis().SetTitleSize(27)
h.GetYaxis().SetLabelFont(43)
h.GetYaxis().SetLabelSize(19)
h.GetYaxis().SetTitleOffset(1)        
h.GetXaxis().SetTitleFont(43)
h.GetXaxis().SetTitleSize(27)
h.GetXaxis().SetLabelFont(43)
h.GetXaxis().SetLabelSize(19)
h.GetXaxis().SetTitleOffset(1)  

c.Update()
for ig,g in enumerate(sorted(gr, reverse=True)):
    print(g)
    gr[g].SetMarkerColor(ig+2)
    if ig ==0:  
        gr[g].Draw('C*')
    else:
        gr[g].Draw('C*')
    leg.AddEntry(gr[g], g.replace('_',' '))
leg.SetLineColor(0)
leg.SetFillStyle(0)
leg.Draw()

text =  R.TLatex()
text.SetNDC()
text.SetTextAlign( 11 )
text.SetTextFont( 42 )
text.SetTextSize( 0.05 )
text.SetTextColor( 1 )
y = 0.85
#write.append(region.replace("_","-"))
for t in write:
    text.DrawLatex(0.15,y, t)
    y = y-0.06
c.Update()

c.SaveAs(pdf_name)
