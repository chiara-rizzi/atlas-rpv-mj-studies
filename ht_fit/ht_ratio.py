import ROOT

ROOT.gROOT.SetBatch()

infile_name='test.root'
infile = ROOT.TFile.Open(infile_name,'READ')

h_ht6j = infile.Get('ht6j')
h_ht6j_highdeta12 = infile.Get('ht6j_highdeta12')
h_ht6j_lowdeta12 = infile.Get('ht6j_lowdeta12')
h_ht = infile.Get('ht')

def make_ratio(h, rebin=0):    
    if rebin>0:
        h.Rebin(rebin)
    h_ratio = h.Clone(h.GetName()+'_ratio')
    h_ratio_shift = h.Clone(h.GetName()+'_ratio')
    h_ratio.Sumw2()
    h_ratio_shift.Sumw2()
    for i in range(-1, h.GetNbinsX()):
        h_ratio_shift.SetBinContent(i, h.GetBinContent(i+1))
        h_ratio_shift.SetBinError(i, h.GetBinError(i+1))
    h_ratio_shift.Divide(h_ratio)
    h_ratio_shift.SetMaximum(2)
    return h_ratio_shift

print('h_ht6j_ratio')
h_ht6j_ratio = make_ratio(h_ht6j,1)
h_ht6j_highdeta12_ratio = make_ratio(h_ht6j_highdeta12,1)
h_ht6j_lowdeta12_ratio = make_ratio(h_ht6j_lowdeta12,1)

print('h_ht_ratio')
h_ht_ratio = make_ratio(h_ht,1)


c = ROOT.TCanvas()
c.cd()
c.SetGrid()
h_ht_ratio.Draw()
c.SaveAs('h_ht_ratio2.pdf')

c2 = ROOT.TCanvas()
c2.cd()
c2.SetGrid()
h_ht6j_ratio.Draw()
c2.SaveAs('h_ht6j_ratio2.pdf')

c3 = ROOT.TCanvas()
c3.cd()
c3.SetGrid()
h_ht6j_highdeta12_ratio.Draw()
c3.SaveAs('h_ht6j_highdeta12_ratio2.pdf')

c4 = ROOT.TCanvas()
c4.cd()
c4.SetGrid()
h_ht6j_lowdeta12_ratio.Draw()
c4.SaveAs('h_ht6j_lowdeta12_ratio2.pdf')
