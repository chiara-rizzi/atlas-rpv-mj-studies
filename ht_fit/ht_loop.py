from __future__ import print_function
import ROOT
import math
import argparse
import json
import os

ROOT.gROOT.SetBatch()

args = argparse.ArgumentParser()
args.add_argument('--folder', default='/eos/atlas/atlascerngroupdisk/phys-susy/RPV_mutlijets_ANA-SUSY-2019-24/ntuples/workshop/FT/mc16a/', help='Frolder with input data')
args.add_argument('--background','-b', help='Name of the input ROOT file with the TTree for background')
args.add_argument('--signal','-s', help='Name of the folder with signal files')
args.add_argument('--json','-j', default='did2group.json', help='Name of the JSON file with DIDS to group')
args.add_argument('--tree','-t', default='trees_SRRPV_', help='Name of the TTree in the ROOT file')
args.add_argument('--output','-o', default='./histograms.root', help='Name of the output ROOT file with the histograms')
args.add_argument('--recreate', action='store_true', help='Recreate output file (default is UPDATE)')
args = args.parse_args()

if args.recreate:
    outfile = ROOT.TFile.Open(args.output,'recreate')
else:
    outfile = ROOT.TFile.Open(args.output,'update')

def fill_and_write_histos(infile_name, tree_name, outfile, suffix='', max_entries=-1):
    infile = ROOT.TFile.Open(infile_name,'READ')
    t = infile.Get(tree_name)
    nentries = t.GetEntries()
    print(nentries)
    
    h_ht6j = ROOT.TH1F('ht6j'+suffix,'ht6j'+suffix,80, 1000,9000)
    h_ht6j_nj6 = ROOT.TH1F('ht6j_nj6'+suffix,'ht6j_nj6'+suffix,80, 1000,9000)
    h_ht6j_nj7 = ROOT.TH1F('ht6j_nj7'+suffix,'ht6j_nj7'+suffix,80, 1000,9000)
    h_ht6j_nj8 = ROOT.TH1F('ht6j_nj8'+suffix,'ht6j_nj8'+suffix,80, 1000,9000)
    h_ht6j_nj9 = ROOT.TH1F('ht6j_nj9'+suffix,'ht6j_nj9'+suffix,80, 1000,9000)
    h_ht_nj6 = ROOT.TH1F('ht_nj6'+suffix,'ht_nj6'+suffix,80, 1000,9000)
    h_ht_nj7 = ROOT.TH1F('ht_nj7'+suffix,'ht_nj7'+suffix,80, 1000,9000)
    h_ht_nj8 = ROOT.TH1F('ht_nj8'+suffix,'ht_nj8'+suffix,80, 1000,9000)
    h_ht_nj9 = ROOT.TH1F('ht_nj9'+suffix,'ht_nj9'+suffix,80, 1000,9000)
    h_ht6j_highdeta12 = ROOT.TH1F('ht6j_highdeta12'+suffix,'ht6j_highdeta12'+suffix,80, 1000,9000)
    h_ht6j_lowdeta12 = ROOT.TH1F('ht6j_lowdeta12'+suffix,'ht6j_lowdeta12'+suffix,80, 1000,9000)
    h_ht = ROOT.TH1F('ht'+suffix,'ht'+suffix,80, 1000,9000)
    h_pt_5_pt_0 = ROOT.TH1F('pt5_over_pt0'+suffix,'pt5_over_pt0'+suffix,80, 0, 1)
    # chiara: write correct binning 
    h_1_over_ptsq = ROOT.TH1F('h_1_over_ptsq'+suffix,'h_1_over_ptsq'+suffix,80,0,0.03)
    
    nentries_loop = nentries if (max_entries<0 or max_entries>nentries) else max_entries
    for ientry in range(0,nentries_loop):
        t.GetEntry(ientry)
        if ientry%100000 == 0: 
            print(ientry,'/',nentries)
        if not t.pass_HLT_ht1000_L1J100: continue    
        nj = t.jet_pt.size()
        if nj < 6: continue
        if t.jet_pt[5] < 50: continue    
        weight = t.mcEventWeight * t.normweight
        ht, ht6j, sum_1_over_ptsq = 0,0,0
        for ij, pt in enumerate(t.jet_pt):
            ht+=pt
            if ij<6: 
                ht6j+=pt
                sum_1_over_ptsq += 1./(pt*pt)
        h_ht.Fill(ht, weight)
        h_ht6j.Fill(ht6j, weight)
        if nj == 6:
            h_ht_nj6.Fill(ht, weight)
            h_ht6j_nj6.Fill(ht6j, weight)
        if nj == 7:
            h_ht_nj7.Fill(ht, weight)
            h_ht6j_nj7.Fill(ht6j, weight)
        if nj == 8:
            h_ht_nj8.Fill(ht, weight)
            h_ht6j_nj8.Fill(ht6j, weight)
        if nj >= 9:
            h_ht_nj9.Fill(ht, weight)
            h_ht6j_nj9.Fill(ht6j, weight)
        if math.fabs(t.jet_eta[0]-t.jet_eta[1])>1.5:
            h_ht6j_highdeta12.Fill(ht6j, weight)
        else:
            h_ht6j_lowdeta12.Fill(ht6j, weight)
        h_pt_5_pt_0.Fill(t.jet_pt[5]/t.jet_pt[0], weight)
        h_1_over_ptsq.Fill(sum_1_over_ptsq, weight)

    outfile.cd()
    h_ht.Write()
    h_ht6j.Write()
    h_ht6j_highdeta12.Write()
    h_ht6j_lowdeta12.Write()
    h_pt_5_pt_0.Write()
    h_1_over_ptsq.Write()
    h_ht_nj6.Write()
    h_ht6j_nj6.Write()
    h_ht_nj7.Write()
    h_ht6j_nj7.Write()
    h_ht_nj8.Write()
    h_ht6j_nj8.Write()
    h_ht_nj9.Write()
    h_ht6j_nj9.Write()


if args.background:
    print('Looking at', args.background)
    file_name=args.folder+args.background
    fill_and_write_histos(file_name, args.tree, outfile, '_qcd')

if args.signal:
    folder = args.folder+args.signal
    f_json = open(args.json)
    dids_dict = json.load(f_json)
    print(dids_dict)
    keys_to_keep = []
    for key in dids_dict:
        if 'UDS' in dids_dict[key] or 'UDB' in dids_dict[key]:
            if float(dids_dict[key].split('_')[-1]) > 800:
                #print(dids_dict[key])
                keys_to_keep.append(key)
    print(keys_to_keep)
    for key in keys_to_keep:
        file_name = folder+'/'+key+'.root'
        if os.path.exists(file_name):            
            suffix = dids_dict[key].replace('GG_rpv_','')
            print('Looking at',suffix)            
            fill_and_write_histos(file_name, args.tree, outfile, suffix)
    f_json.close

outfile.Close()

