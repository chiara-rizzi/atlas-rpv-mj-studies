from __future__ import print_function
import ROOT
import math
import argparse

ROOT.gROOT.SetBatch()
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)

c = ROOT.TCanvas("c1","c1", 600, 600)

args = argparse.ArgumentParser()
args.add_argument('--pdf', default='h_ht6j_fit.pdf', help='Name of the output file for the plot')    
args.add_argument('--infile','-i', default='histograms.root', help='Name of the input ROOT file with the histograms')
args.add_argument('--histogramName','-n', default='ht6j', help='Name of the histogram as in the ROOT file. E.g. ht6j, ht6j_highdeta12, ht6j_lowdeta12')
args.add_argument('--unblind','-u', action='store_true', help='Look at data in SR')
args.add_argument('--signal','-s', action='store_true', help='Check signal contamination')
args = args.parse_args()

histo_names = ['ht_nj6_qcd','ht_nj7_qcd','ht_nj8_qcd','ht_nj9_qcd']
pdf_name = 'ht_plots.pdf'

infile = ROOT.TFile.Open(args.infile,'READ')
histos = []
histos_ratio = []
for ih,name in enumerate(histo_names):
    h = infile.Get(name)
    h.SetDirectory(0)
    histos.append(h)

for ih,h in enumerate(histos):
    for i in range(11):
        h.SetBinContent(i,0)
    for i in range(51,90):
        h.SetBinContent(i,0)
    h.Scale(139000)
    h.Scale(1./h.Integral())
    h.Rebin(4)
for ih,h in enumerate(histos):
    histos_ratio.append(h.Clone(h.GetName()+'ratio'))
    histos_ratio[ih].Divide(histos[0])

colors = [609, 856, 410, 801, 629, 879, 602, 921, 622]
c.cd()
pad1 = ROOT.TPad("pad1", "pad1",0.0,0.35,1.0,1.0,21)
pad2 = ROOT.TPad("pad2", "pad2",0.0,0.0,1.0,0.35,22)
pad2.SetGridy()
pad1.SetFillColor(0)
pad1.SetLogy()
pad2.SetFillColor(0)
pad1.Draw()
pad2.Draw()

leg=ROOT.TLegend(0.7,0.68,0.92,0.89)            
leg.SetFillStyle(0)
leg.SetLineColor(0)
for ih,myh in enumerate(histos):
    leg.AddEntry(myh,histo_names[ih] , "l")

h_max = max([h.GetMaximum() for h in histos])

for ih, h in enumerate(histos):    
    pad1.cd()
    h.SetLineColor(colors[ih])
    h.GetXaxis().SetTitle('HT [GeV]')
    h.SetMaximum(1.5*h_max)
    h.Draw('same')
    leg.Draw()
    pad2.cd()
    histos_ratio[ih].SetLineColor(colors[ih])
    histos_ratio[ih].SetMinimum(0.5)
    histos_ratio[ih].SetMaximum(1.5)
    histos_ratio[ih].GetYaxis().SetTitle('Ratio to first histo')
    histos_ratio[ih].Draw('same')
c.SaveAs(pdf_name)

