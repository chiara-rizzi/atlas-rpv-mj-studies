from __future__ import print_function
import ROOT
import math
import argparse

ROOT.gROOT.SetBatch()
c = ROOT.TCanvas("c1","c1")

args = argparse.ArgumentParser()
args.add_argument('--sr', default=[], nargs=2, type=float,
                  help='high and low bundaries for SR. It needs to be a multiple of the bin width of the histogram!')
args.add_argument('--vr', type = float, default=0.,
                  help='Width of the region between SR and fit region. It needs to be a multiple of the bin width of the histogram!')
args.add_argument('--fit', default=[2000, 6000], nargs=2, type=float,
                  help='high and low bundaries for the fit. It needs to be a multiple of the bin width of the histogram!')
args.add_argument('--pdf', default='h_ht6j_fit.pdf', help='Name of the output file for the plot')    
args.add_argument('--infile','-i', default='histograms.root', help='Name of the input ROOT file with the histograms')
args.add_argument('--histogramName','-n', default='ht6j', help='Name of the histogram as in the ROOT file. E.g. ht6j, ht6j_highdeta12, ht6j_lowdeta12')
args.add_argument('--unblind','-u', action='store_true', help='Look at data in SR')
args.add_argument('--signal','-s', action='store_true', help='Check signal contamination')
args.add_argument('--injection','-j', action='store_true', help='Perform signal injection')
args = args.parse_args()


infile = ROOT.TFile.Open(args.infile,'READ')
h_ht6j = infile.Get(args.histogramName + '_qcd')
h_ht6j.Scale(139000.)
if args.signal:
    h_ht6j_dict_sig = {}
    #list_sig = ['UDS_900', 'UDS_1000', 'UDS_1100', 'UDS_1200',  'UDS_1500', 'UDS_1600', 'UDS_1700', 'UDS_1800','UDS_1900','UDS_2000', 'UDS_2100', 'UDS_2200', 'UDS_2300', 'UDS_2400',  'UDS_2500']
    list_sig = ['UDS_900', 'UDS_1200', 'UDS_1600', 'UDS_2100']
    for sig in list_sig:
        h = infile.Get(args.histogramName + sig)
        if h:
            h.Scale(139000.)
            h.SetDirectory(0)
        h_ht6j_dict_sig[sig] =  h

exclude_low = args.sr[0] - args.vr
exclude_high = args.sr[1] + args.vr

reject = True

def pyf_tf1_expo(x, p):
    if reject and x[0] > exclude_low and x[0] < exclude_high:
        ROOT.TF1.RejectPoint()
        return 0
    return math.exp(p[0]+p[1]*x[0])


def fit_hist(h, xmin, xmax, rebin=0):    
    #h.Sumw2()
    if rebin>0:
        h.Rebin(rebin)
    #h.Fit("expo","","",xmin,xmax)
    #myexp = h.GetFunction("expo");
    #print(myexp.GetParameter(0))
    funexp = ROOT.TF1("myexp", pyf_tf1_expo, xmin, xmax, 2);
    funexp.SetParameters(2.7, -0.002)
    h.Fit(funexp, "q","",xmin,xmax)
    myexp = h.GetFunction("myexp")
    #print('ok')
    return h, myexp

def extrapolate_fun(myfun, myh, xmin, xmax, unblind=False):
    integral = 0
    integral_histo = 0
    error = 0
    nbins = 0
    for ibin in range(myh.GetNbinsX()+1):
        if myh.GetBinLowEdge(ibin) < xmax and (myh.GetBinLowEdge(ibin)+ myh.GetBinWidth(ibin)>xmin):
            #print('Considering bin from ', myh.GetBinLowEdge(ibin) , 'to', myh.GetBinLowEdge(ibin)+myh.GetBinWidth(ibin))
            bin_center = myh.GetBinCenter(ibin)
            integral += myfun.Eval(bin_center)
            integral_histo += myh.GetBinContent(ibin)
            nbins += 1    
    print('Integral function:', "{:.2f}".format(integral))
    if unblind:
        print('Integral histogram:', "{:.2f}".format(integral_histo))
        print('Relative difference:', "{:.2f}".format(100.*(integral-integral_histo)/integral),'%')
    return integral, error

def get_integral(myh, xmin, xmax):
    integral = 0
    error = 0
    nbins = 0
    for ibin in range(myh.GetNbinsX()+1):
        if myh.GetBinLowEdge(ibin) < xmax and (myh.GetBinLowEdge(ibin)+ myh.GetBinWidth(ibin)>xmin):
            integral += myh.GetBinContent(ibin)
            nbins += 1
    return integral, error 

# print('Histogram:', args.histogramName)
# fit with exp(p0+p1*x)
print('Fit in:', args.fit[0], '-', args.fit[1], 'GeV')
if exclude_low<args.fit[1]:
    print('Excluded from fit:', exclude_low, '-', exclude_high, 'GeV')
h_ht6j_fit, myexp_ht6j = fit_hist(h_ht6j, args.fit[0], args.fit[1])
reject=False
if args.vr >0:
    print('VR low:', args.sr[0]-args.vr, '-',  args.sr[0], 'GeV')
    myint_vr1, myint_err_vr1 = extrapolate_fun(myexp_ht6j, h_ht6j, args.sr[0]-args.vr, args.sr[0], unblind=True)
    print('VR high:', args.sr[1], '-',  args.sr[1]+args.vr, 'GeV')
    myint_vr2, myint_err_vr2 = extrapolate_fun(myexp_ht6j, h_ht6j, args.sr[1], args.sr[1]+args.vr, unblind=True)
print('SR:', args.sr[0],'-' ,args.sr[1], 'GeV')
myint, myint_err = extrapolate_fun(myexp_ht6j, h_ht6j, args.sr[0], args.sr[1], unblind=args.unblind)

if args.signal:
    for sig in list_sig: # loop on the list to have them ordered
        if sig in h_ht6j_dict_sig: 
            sig_yields, sig_err = get_integral(h_ht6j_dict_sig[sig], args.sr[0], args.sr[1])
            print(sig, "S/B:", "{:.2f}".format(sig_yields/myint), '  S:', "{:.2f}".format(sig_yields), '  B:', "{:.2f}".format(myint))
    if args.injection:
        print('\nSignal injection test')
        for sig in list_sig:
            if sig in h_ht6j_dict_sig:
                print('\n'+sig)
                h_ht6j_with_sig = h_ht6j.Clone('h_with_'+sig)
                h_ht6j_with_sig.Add(h_ht6j_dict_sig[sig])
                reject = True
                h_ht6j_with_sig, myexp_ht6j_with_sig = fit_hist(h_ht6j_with_sig, args.fit[0], args.fit[1])
                reject = False
                if args.vr >0:
                    print('VR low')
                    myint_vr1_with_sig, myint_err_vr1_with_sig = extrapolate_fun(myexp_ht6j_with_sig, h_ht6j_with_sig, args.sr[0]-args.vr, args.sr[0], unblind=True)
                    print('VR high')
                    myint_vr2_with_sig, myint_err_vr2_with_sig = extrapolate_fun(myexp_ht6j_with_sig, h_ht6j_with_sig, args.sr[1], args.sr[1]+args.vr, unblind=True)
                print('SR')
                myint_with_sig, myint_err_with_sig = extrapolate_fun(myexp_ht6j_with_sig, h_ht6j_with_sig, args.sr[0], args.sr[1], unblind=args.unblind)
                sig_yields, sig_err = get_integral(h_ht6j_dict_sig[sig], args.sr[0], args.sr[1])
                print(sig, "S/B:", "{:.2f}".format(sig_yields/myint_with_sig), '  S:', "{:.2f}".format(sig_yields), '  B:', "{:.2f}".format(myint_with_sig))
        #print('Integral:', myint, '+-', myint_err)



c.cd()
c.SetGrid()
c.SetLogy()
h_ht6j.Draw()
c.SaveAs('h_ht6j_fit.pdf')

