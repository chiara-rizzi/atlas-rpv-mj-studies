# Studies for RPV MJ analysis 
Author: Chiara Rizzi 

This repository contains scripts I've used to perform various studies for the RPV MJ analysis. 

## ht_fit

The `ht_fit` folder contains the scripts to perform studies on the exponential fit of the HT distribution. 
The scripts are fairly flexible and the options are passed through the command line. 
These scripts have been used to produce the results presented [here](https://indico.cern.ch/event/1118713/#7-ht-fit). 

[ht_loop.py](ht_fit/ht_loop.py) loops on the ntuples and fills the histograms. 
The histograms to be filled as well as the preselections are hardcoded inside the scripts, but several other parameters 
(such as the name of the input file, the name of the TTree, the name of the output file, ...) are command-line options. 
Example usage: 
```python
python ht_loop.py -b qcd_cleaned.root --recreate -s signal
```

[ht_ratio.py](ht_fit/ht_ratio.py) takes the ROOT file creates by [ht_loop.py](ht_fit/ht_loop.py) and 
performs the ratio of neighboring bins. This ratio should be flat if the distribution is exponential. 
This is not used for the fit, but just to visualize easily if the distribution is indeed exponential, and 
in which range. 

[ht_fit.py](ht_fit/ht_fit.py) performs the exponential fit of the histograms created in [ht_loop.py](ht_fit/ht_loop.py). 
It is possible to choose the region where to perform the fit, the region where to evaluate. The signal contamination is also computed. 
It is also possible to perform signal-injection tests to evaluate the effect of the presence of a signal in the fit region. 
Example usage: 
```
python ht_fit.py --sr 5000 5400 --vr 400 -n ht -u -s -j
```

[plot_histo.py](ht_fit/plot_histo.py) plots some of the histograms created with [ht_loop.py](ht_fit/ht_loop.py), normalized to unit area or to the 
expected luminosity. The histograms to be plotted are hardcoded. 


## for_FT_ntuples

The `for_FT_ntuples` folder contains scripts to do quick studies on the ntuples produced with FactoryTools. 
These scripts are in general quite hardcoded. 

[dijet_plot.py](for_FT_ntuples/dijet_plot.py) produces plots of the distributions of the variables in the FT ntuples 
for the dijet samples. There's the possibility to visualize the contribution of the different slices separately, and to 
produce data/MC comparisons (used to produce the plots on
slides 7-8 [here](https://indico.cern.ch/event/997190/contributions/4232152/attachments/2190167/3701480/21_02_15_chiara.pdf)).

<img src="example_plots/leading_jet_pt_data_mc_100_1100.png" alt="data_mc" width="400"/>

[signal_eff.py](for_FT_ntuples/signal_eff.py) computes the efficiency of a selection (hardcoded in the script) in 
the signal samples, as a function of the gluino mass. Useful e.g. for trigger efficiency studies (used to produce the plots on 
slides 4-5-6 [here](https://indico.cern.ch/event/997190/contributions/4232152/attachments/2190167/3701480/21_02_15_chiara.pdf)).

<img src="example_plots/eff_trigger.png" alt="plot_trigger" width="400"/>

[clean_qcd.py](for_FT_ntuples/clean_qcd.py) prepares the `qcd_cleaned.root` files starting from the `qcd.root` file produced with 
FactoryTools. The cleaned version requires `'(((jet_pt[0]+jet_pt[1])/2.) < 1.4*truth_jet_pt[0])'`. 

[correlation_ntuples.py](for_FT_ntuples/correlation_ntuples.py) prints the correlation of variables in the ntuples. 

### 2x5 model 

Two scripts in `for_FT_ntuples` perform the studies on the angular separation of the decay products of the gluino and of the neutralino in the 
case of the 2x5 model. These are [check_dR_gluino_decay.py](for_FT_ntuples/check_dR_gluino_decay.py), to check the angular separation of 
the decay products of the gluino, and [check_dR_neutralino_decay.py](for_FT_ntuples/check_dR_neutralino_decay.py), for the decay products of the 
neutralino. These scripts have been used e.g. to produce the results in [these slides](https://indico.cern.ch/event/1023170/contributions/4297478/attachments/2217526/3754798/21_03_29_10jets_signal_chiara.pdf). 

## for_alpaca_ntuples

The folder `for_alpaca_ntuples` contains scripts that run on the ntuples produced with the `gluglu` analysis in the alpaca framework. 
Please note that this ntuple format is not generic for all the alpaca analyses, but it's specific for the analysis carried out 
with the [scripts](https://gitlab.cern.ch/atlas-phys-susy-wg/RPVLL/rpvmultijet/alpaca/-/tree/master/alpaca/analyses/gluglu/scripts) for the 
`gluglu` analysis. 

[plot_after_alpaca.py](for_alpaca_ntuples/plot_after_alpaca.py) plots distributions of the variables in the ntuples. 
This script has been used e.g. to produce [these studies](https://indico.cern.ch/event/997191/contributions/4242903/attachments/2194414/3709739/21_02_22_chiara.pdf). 

<img src="example_plots/jet_pt_0_with_weights_unit_area.png" alt="plot_alpaca" width="400"/>

[correlation.py](for_alpaca_ntuples/correlation.py) prints the correlation of some of the variables in the ntuples. 






